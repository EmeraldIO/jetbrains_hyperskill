package tictactoe;

import java.util.Scanner;
import java.util.StringJoiner;

public class Main {

    String [][] cells = new String[3][3];
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        StringBuilder res = new StringBuilder("---------\n");
        StringBuilder builder = new StringBuilder("| ");
        StringJoiner joiner = new StringJoiner(" ");
        int counter = 1;
        for (int i = 0; i < line.length(); i++) {
            joiner.add(line.charAt(i) + "");
            builder.append(line.charAt(i)).append(" ");
            if (counter % 3 == 0) {
                builder.append("|\n");
                res.append(builder);
                builder = new StringBuilder("| ");
            }
            counter++;
        }
        System.out.println(res.append("---------"));
        String[] splited = joiner.toString().split("\\s");
        int x = 0;
        int o = 0;
        int empty = 0;
        for (int i = 0; i < splited.length; i++) {
            if (splited[i].equals("X")) {
                x++;
            } else if (splited[i].equals("O")) {
                o++;
            } else {
                empty++;
            }
        }
        Main main = new Main();

        int count=0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                main.cells[i][j] = line.charAt(count) + "";
                ++count;
            }
        }


        int diff = Math.abs(x-o);
        if((diff)>1){
            System.out.println("Impossible");
        }else{
            boolean isX = main.isWin("X");
            boolean isO = main.isWin("O");
            if(isX && isO){
                System.out.println("Impossible");
            }
            else if(isX){
                System.out.println("X wins");
            }else if(isO){
                System.out.println("O wins");
            }else if(empty>2 && !isO && !isX){
                System.out.println("Game not finished");
            }else{
                System.out.println("Draw");
            }
        }
    }

    public boolean isWin(String ch) {
        // row win

        return cells[0][0].equals(ch) && cells[0][1].equals(ch) && cells[0][2].equals(ch)
                || cells[1][0].equals(ch) && cells[1][1].equals(ch) && cells[1][2].equals(ch)
                || cells[2][0].equals(ch) && cells[2][1].equals(ch) && cells[2][2].equals(ch)
                // column win
                || cells[0][0].equals(ch) && cells[1][0].equals(ch) && cells[2][0].equals(ch)
                || cells[0][1].equals(ch) && cells[1][1].equals(ch) && cells[2][1].equals(ch)
                || cells[0][2].equals(ch) && cells[1][2].equals(ch) && cells[2][2].equals(ch)
                // diagonal win
                || cells[0][0].equals(ch) && cells[1][1].equals(ch) && cells[2][2].equals(ch)
                || cells[2][0].equals(ch) && cells[1][1].equals(ch) && cells[0][2].equals(ch);
    }


}