import java.util.*;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = new ArrayList<>();
        while (scanner.hasNext()) {
            int number = scanner.nextInt();
            if (number == 0) {
                break;
            }
            else{
                list.add(number);
            }
        }
        List<Integer>sortedaAsc = new ArrayList<>(list);
        List<Integer>sortedaDsc = new ArrayList<>(list);
        Collections.sort(sortedaAsc);
        Collections.sort(sortedaDsc,Collections.reverseOrder());
        if(sortedaAsc.equals(list)||sortedaDsc.equals(list)){
            System.out.println("true");
        }else{
            System.out.println("false");
        }

    }
}