import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int h = scanner.nextInt();
        int up = scanner.nextInt();
        int down = scanner.nextInt();
        int temp = 0;
        int steps = 0;
        while (temp != h) {
            temp = (temp + up);
            if (temp >= h) {
                System.out.println(steps + 1);
                break;
            } else {
                temp -= down;
                steps++;
            }
        }
    }
}