//put imports you need here

import java.util.*;
import java.util.stream.Stream;

class Main {
    public static void main(String[] args) {
/*        Scanner scanner = new Scanner(System.in);
        String s1 = scanner.nextLine();
        String s2 = scanner.nextLine();
        String s3 = scanner.nextLine();
        String s4 = scanner.nextLine();

        for (int i =  s4.split(" ").length; i > = 0;i--) {
            System.out.println();
        }*/

        Scanner scanner = new Scanner(System.in);
        String s1 = scanner.nextLine();
        String s2 = scanner.nextLine();
        String s3 = scanner.nextLine();
        String s4 = scanner.nextLine();
        List<String> list = new ArrayList<String>(){{
            addAll(Arrays.asList(s1.split(" ")));
            addAll(Arrays.asList(s2.split(" ")));
            addAll(Arrays.asList(s3.split(" ")));
            addAll(Arrays.asList(s4.split(" ")));
        }};
        Collections.reverse(list);
        list.forEach(System.out::println);
//        String s5 = s1 +" "+ s2 +" "+ s3 +" "+ s4;
//        String[] array = s5.split(" ");
//        for (int i = array.length-1; i >=0; i--) {
//            System.out.println(array[i]);
//        }

    }
}