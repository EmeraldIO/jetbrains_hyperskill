import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s1 = scanner.nextLine();
        String s2 = scanner.nextLine();
        String s3 = scanner.nextLine();

        List<String> list = new ArrayList<String>(){{
            addAll(Arrays.asList(s1.split(" ")));
            addAll(Arrays.asList(s2.split(" ")));
            addAll(Arrays.asList(s3.split(" ")));
        }};
        list.forEach(System.out::println);
    }
}