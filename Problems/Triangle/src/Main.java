import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        int ab = a + b;
        int ac = a + c;
        int bc = b + c;
        if ((ab > c) && (ac > b) && (bc > a)) {
            System.out.println("YES");
        }else{
            System.out.println("NO");
        }
    }
}