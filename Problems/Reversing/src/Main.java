import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        StringBuilder stringBuilder = new StringBuilder(a+"").reverse();
        System.out.println(Integer.valueOf(stringBuilder.toString()));
    }
}