import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int duration  = scanner.nextInt();
        int foodCost  = scanner.nextInt();
        int flightCost  = scanner.nextInt();
        int costPerNight  = scanner.nextInt();

        System.out.println((duration*foodCost)+((duration-1)*costPerNight)+(flightCost*2));
    }
}